import numpy as np
import logging
from behavior import Behavior

log = logging.getLogger('AvoidObstacleBehavior')

class AvoidObstacleBehavior(Behavior):
    def __init__(self, controller):
        self.controller = controller
        self.robot = self.controller.robot

    def execute(self):
        log.debug("Avoid Obstacle")

        angle = self.controller.get_angle_of_min_sensor_dist()
        log.debug(f"Too close at angle: {np.rad2deg(angle)}")
        too_close_dist = self.controller.too_close_dist
        escape_dist = too_close_dist
        if angle > np.pi/16:
            dist = escape_dist * 1.5
            log.debug(f"Avoid sideways : {dist}")
            self.controller.move_dist_sideways_controller(dist)
        elif angle < np.pi/16:
            dist = -escape_dist * 1.5
            log.debug(f"Avoid sideways : {dist}")
            self.controller.move_dist_sideways_controller(dist)
        else:
            dist = -escape_dist
            log.debug(f"Avoid reverse: {dist}")
            self.controller.move_dist_controller(dist)
