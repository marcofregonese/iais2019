import time
import vrep
import numpy as n
import utils as Utils
from vrepobject import Vrepobject
import logging
log = logging.getLogger('RobotArm')
log.setLevel(logging.DEBUG)


class RobotArm(object):

    """Docstring for RobotArm. """

    def __init__(self, client_id):
        """TODO: to be defined1. """
        self.client_id = client_id
        self.arm_rot = Vrepobject(client_id, 'Joint')
        self.base = Vrepobject(client_id, 'youBotArmJoint0')
        self.shoulder = Vrepobject(client_id, 'youBotArmJoint1')
        self.ellbow = Vrepobject(client_id, 'youBotArmJoint2')
        self.wrist = Vrepobject(client_id, 'youBotArmJoint3')
        self.gripper_rot = Vrepobject(client_id, 'youBotArmJoint4')
        self.l_upper_arm = 0.155
        self.l_lower_arm = 0.135
        self.l_hand = 0.2175
        self.arm_center = Vrepobject(client_id, 'ME_Arm1_m0_sub0_sub0')
        self.arm_joints = [self.shoulder, self.ellbow, self.wrist]
        self.all_joints = [self.shoulder,
                           self.ellbow,
                           self.wrist,
                           self.arm_rot]
        self.gripper_tip = Vrepobject(client_id, 'youBot_gripperPositionTip')

        # Set arm to startig configuration
        self.reset_angles = [n.deg2rad(30.91),
                             n.deg2rad(52.42),
                             n.deg2rad(30),
                             n.deg2rad(0)]

        self.zero_angles = [n.deg2rad(0),
                            n.deg2rad(0),
                            n.deg2rad(0),
                            n.deg2rad(0),
                            n.deg2rad(0)]

    def reset_arm_position(self):
        res = vrep.simxPauseCommunication(self.client_id, True)
        Vrepobject.check_for_errors(res, "rotate_joint:simxPauseCommunication")
        for i in range(0, len(self.all_joints)):
            self._set_joint_angle(self.all_joints[i],
                                  self.reset_angles[i])
            Vrepobject.check_for_errors(res, "rotate_joint:simxPauseComm")
        res = vrep.simxPauseCommunication(self.client_id, False)
        time.sleep(2)

    def set_arm_angles(self, arm_angles):
        res = vrep.simxPauseCommunication(self.client_id, True)
        Vrepobject.check_for_errors(res, "rotate_joint:simxPauseCommunication")
        for i in range(0, 3):
            self._set_joint_angle(self.arm_joints[i],
                                  arm_angles[i])
            Vrepobject.check_for_errors(res, "rotate_joint:simxPauseComm")

        res = vrep.simxPauseCommunication(self.client_id, False)
        Vrepobject.check_for_errors(res, "rotate_joint:simxPauseCommunication")
        time.sleep(3)

    def set_arm_angle(self, arm_joint, arm_angle):
        res = vrep.simxPauseCommunication(self.client_id, True)
        Vrepobject.check_for_errors(res, "pause_comm:simxPauseCommunication")

        self._set_joint_angle(arm_joint, arm_angle)
        res = vrep.simxPauseCommunication(self.client_id, False)
        Vrepobject.check_for_errors(res, "pause_comm:simxPauseCommunication")

    def _set_joint_angle(self, joint, radians):
        res = vrep.simxSetJointTargetPosition(self.client_id,
                                              joint.get_handle(),
                                              radians,
                                              vrep.simx_opmode_streaming)
        Vrepobject.check_for_errors(res, "rotate_joint:simxSetJointTargetPosition")

    def set_arm_orientation(self, radians):
        self._set_joint_angle(self.base, radians)
        time.sleep(2)

    def open_gripper(self):
        res = vrep.simxSetIntegerSignal(self.client_id,
                                        'gripper_open',
                                        1,
                                        vrep.simx_opmode_oneshot)
        Vrepobject.check_for_errors(res, "open_gripper:simxSetIntegerSignal")

    def close_gripper(self):
        res = vrep.simxSetIntegerSignal(self.client_id,
                                        'gripper_open',
                                        0,
                                        vrep.simx_opmode_oneshot)
        Vrepobject.check_for_errors(res, "open_gripper:simxSetIntegerSignal")
        time.sleep(2)

    def pick_up_obj(self, obj_pos):
        pos = Utils.vec2Dto3D(obj_pos)
        self.open_gripper()
        time.sleep(1)
        self.move_arm_to(pos)
        self.close_gripper()
        time.sleep(1)
        self.reset_arm_position()
        self.set_arm_orientation(0)

    def move_arm_to(self, target_pos):
        arm_pos = self.arm_center.get_position()
        rel_target_pos = n.array(arm_pos) - n.array(target_pos)
        log.debug(f"arm_pos {arm_pos}")
        log.debug(f"target_pos {target_pos}")
        log.debug(f"rel_target_pos {rel_target_pos}")

        # rotate in direction of target
        self.rotate_arm_to_object(target_pos)

        # move arm to pos
        arm_angles = self.get_arm_angles_to_pos(rel_target_pos)
        self.set_arm_angles(arm_angles)
        gripper_pos = self.gripper_tip.get_position()
        log.debug(f"Gripper_pos: {gripper_pos}")
        log.debug(f"Target pos: {target_pos}")

    def rotate_arm_to_object(self, target_pos):
        angle = n.deg2rad(-63)
        self.set_arm_orientation(angle)
        time.sleep(3)
        log.debug(f"Target pos: {target_pos}")

    def get_distance_to(self, pos):
        return Utils.get_pythagoras_distance(pos[0], pos[1])

    def get_arm_angles_to_pos(self, rel_target_pos):
        phi_shoulder = 1.204799
        phi_ellbow = 0.925696
        phi_wrist = 0.225698
        """
        dist_to_target = self.get_distance_to(rel_target_pos)
        L1 = self.l_upper_arm   # Length of 1st arm-part
        L2 = self.l_lower_arm   # Length of 2nd arm-part
        L3 = self.l_hand        # Length of 3rd arm-part
        log.debug(f"L1 = {L1}, L2 = {L2}, L3 = {L3}")

        AG = n.deg2rad(-45)        # actuation of gripper
        theta3 = AG

        # Position of Gripper Tip (wT, zT)
        wT = dist_to_target + 0.06
        zT = -0.17
        log.debug(f"(wT, zT) = ({wT},{zT})")

        # position of wrist-joint (w2, z2)
        w2 = wT - L3 * n.cos(theta3)
        z2 = zT - L3 * n.sin(theta3)
        log.debug(f"z2 = {zT} - {L3} * {n.sin(theta3)}")
        log.debug(f"(w2, z2) = ({w2},{z2})")

        # Helper angles
        theta12 = n.arctan(z2/w2)
        log.debug(f"theta12 {theta12} ({n.rad2deg(theta12)})")
        L12 = n.sqrt(n.square(w2) + n.square(z2))
        if L12 > L1 + L2:
            raise Exception(f"{L12} > {L1} + {L2}:\
                    Target too far away. I can't reach it")
        # Law of cosine
        # L2^2 = L1^2 + L12^2 - 2 * L1 * L12 * cos(alpha)
        cos_alpha = (L1**2 + L12**2 - L2**2) / (2 * L1 * L12)
        alpha = n.arccos(cos_alpha)
        log.debug(f"alpha {alpha} ({n.rad2deg(alpha)})")

        # Position of ellbow-joint (w1, z1)
        theta1 = theta12 + alpha
        log.debug(f"theta1 {theta1} ({n.rad2deg(theta1)})")
        w1 = L1 * n.cos(theta1)
        z1 = L1 * n.sin(theta1)
        log.debug(f"(w1, z1) = ({w1},{z1})")

        theta2 = n.arctan((z2 - z1)/(w2 - w1))
        log.debug(f"theta2 {theta2} ({n.rad2deg(theta2)})")

        phi_shoulder = n.pi/2 - theta1
        phi_ellbow = theta1 - theta2
        phi_wrist = theta2 - theta3
        log.debug(f"phi_shoulder = {phi_shoulder} ({n.rad2deg(phi_shoulder)})")
        log.debug(f"phi_ellbow = {phi_ellbow} ({n.rad2deg(phi_ellbow)})")
        log.debug(f"phi_wrist = {phi_wrist} ({n.rad2deg(phi_wrist)})")

        """ 
        return phi_shoulder, phi_ellbow, phi_wrist

    def get_goal_direction(self, goal_pos):
        a_b_g = self.arm_center.get_orientation()
        g = a_b_g[2]
        quadrant = self.get_quadrant_wrt(goal_pos)
        dist_x, dist_y, dist_abs = self.get_triangle_to(goal_pos)
        # log.debug("x ", dist_x, " y ", dist_y, " distance ", dist_abs)
        if quadrant == 1:
            angle = n.pi - n.arcsin(dist_x/dist_abs) - g
        elif quadrant == 2:
            angle = - n.pi + n.arcsin(dist_x/dist_abs) - g
        elif quadrant == 3:
            angle = - n.arcsin(dist_x/dist_abs) - g
        elif quadrant == 4:
            angle = n.arcsin(dist_x/dist_abs) - g
        # log.debug("Angle towards goal:", angle*360/(2*n.pi))
        return angle

    def get_triangle_to(self, pos_goal):
        """
        returns, dist_x, dist_y, and abs_dist (triangle) to pos_goal
        """
        pos_robot = self.arm_center.get_position()
        target_relative_to_robot = n.array(pos_goal) - n.array(pos_robot)
        # Distance in x direction
        dist_x = target_relative_to_robot[0]
        # Distance in x direction
        dist_y = target_relative_to_robot[1]
        # Absolute distance ( d = sqrt(x^2 + y^2))
        dist_abs = Utils.get_pythagoras_distance(dist_x, dist_y)
        return dist_x, dist_y, dist_abs

    def get_quadrant_wrt(self, pos_goal):
        pos_robot = self.arm_center.get_position()
        rx = pos_robot[0]
        ry = pos_robot[1]
        tx = pos_goal[0]
        ty = pos_goal[1]
        res = (rx*-1+tx, ry*-1+ty)
        quadrant = 0
        if res[0] >= 0 and res[1] >= 0:
            quadrant = 1
        elif res[0] < 0 and res[1] >= 0:
            quadrant = 2
        elif res[0] < 0 and res[1] < 0:
            quadrant = 3
        elif res[0] >= 0 and res[1] < 0:
            quadrant = 4
        return quadrant
