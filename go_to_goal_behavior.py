import numpy as np
import logging

log = logging.getLogger('GoToGoalBehavior')


class GoToGoalBehavior:
    def __init__(self, controller):
        self.controller = controller
        self.robot = self.controller.robot
        # PD-Controller
        # how fast we turn in proportion to how far away
        self.kP = 1.0
        self.kD = 1.0
        self.goal_pos = [0, 0]

    def set_goal_pos(self, goal_pos):
        self.goal_pos = goal_pos

    def execute(self):
        ctrl = self.controller
        goal_pos = ctrl.current_goal

        # Angle to rotate for being alignd with goal
        rot_angle = ctrl.get_direction_to(goal_pos)
        # Front (direct) distance to goal
        dist_to_goal = ctrl.distance(goal_pos)
        log.debug(f"rot_angle {np.rad2deg(rot_angle)}")
        log.debug(f"dist_to_goal {dist_to_goal}")

        min_collision_dist = self.controller.min_collision_dist
        ctrl.rotate_angle_controller(rot_angle)
        # check sensor data
        ctrl._update_sensor_data()
        _, dist_center, _ = ctrl.get_sensor_distances_lcr()
        if(dist_center < dist_to_goal):
            ctrl.move_dist_controller(dist_center)
        else:
            ctrl.move_dist_controller(dist_to_goal - min_collision_dist)
