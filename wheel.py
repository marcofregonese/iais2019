from vrepobject import Vrepobject
import vrep


class Wheel(Vrepobject):
    """ Represents One Wheel(joint) """
    def __init__(self, client_id, name):
        Vrepobject.__init__(self, client_id, name)

    def setSpeed(self, speed):
        vrep.simxSetJointTargetVelocity(
            self.client_id,
            self.handle,
            speed,
            vrep.simx_opmode_oneshot)

