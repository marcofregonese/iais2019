import numpy as np
import logging
from behavior import Behavior

log = logging.getLogger('FollowWallBehavior')


class FollowWallBehavior(Behavior):
    def __init__(self, controller):
        self.controller = controller
        self.robot = self.controller.robot
        # PD-Controller
        # how fast we turn in proportion to how far away
        self.kP = 1.0
        self.kD = 1.0
        self.direction = "cw"
        self.min_collision_dist = 0.5
        # for direction
        self.cw = -1
        self.ccw = 1

    def execute(self):
        log.debug("Follow wall")
        distances = self.controller.get_sensor_distances_lcr()
        sign = self.cw if self.controller.direction == "cw" else self.ccw

        if self.isInnerCorner(distances):
            # if corner (obstacle ends)
            log.debug("Corner")
            angle = np.deg2rad(90*sign)
        else:
            # adjust direction if necessary
            angle = self.adjust_direction(distances)

        self.controller.rotate_angle_controller(angle)
        self.controller.move_dist_controller(0.05)

    def isCorner(self, dist):
        return self.isInnerCorner(dist) or self.isOuterCorner(dist)

    def isInnerCorner(self, distances):
        d_front = distances[2]
        result = d_front < self.min_collision_dist - 0.2
        if result:
            log.debug("Wall In Front")
        return result

    def isOuterCorner(self, distances):
        d1 = distances[0]
        d2 = distances[1]
        result = d1 > 1 or d2 > 1
        if result:
            log.debug("Outer Corner")
        return result

    def adjust_direction(s, distances):
        log.debug("Adjust direction")
        d1 = distances[0]
        d2 = distances[1]
        distance = (d1 + d2) / 2
        omega = np.pi/8

        log.debug(f"Distance to wall: {distance}")
        if (distance < s.min_collision_dist - 0.2):
            log.debug("Too close, rotate away from obstacle")
            # if dir == RIGHT => obstacle left of robot => turn away = turn cw
            sign = s.cw if s.direction == "cw" else s.ccw
            return omega * sign
        if (distance > s.min_collision_dist + 0.2):
            log.debug("Too far, rotate towards obstacle")
            sign = s.ccw if s.direction == "cw" else s.cw
            return omega * sign
        return 0
