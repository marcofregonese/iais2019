
import logging 
log = logging.getLogger('Behavior')


class Behavior:
    def execute(self):
        log.error("execute() is not defined")
        exit(0)
