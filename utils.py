import numpy as np


def get_pythagoras_distance(a, b):
        """
        Returns lengt of hypothenuse given length of two sides of 
        right triangle 
        :param a: length of one side of triangle
        :param b: length of other side of triangle
        """
        dist_abs = np.sqrt(np.square(a) + np.square(b))
        return dist_abs


def unit_vector(vector):
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def vec2Dto3D(vec2d):
    """
    Appends 0 to a 2d-Vector [x, y] -> [x, y, 0]
    """
    return [vec2d[0], vec2d[1], 0]


def homogeneous2Dto3D(vec2d):
    """
    Appends 1 to a 2d-Vector [x, y] -> [x, y, 1]
    """
    return [vec2d[0], vec2d[1], 1]


def isEmpty(obj):
    result = obj is None
    result = result or (obj == [])
    result = result or (obj == {})
    return result
