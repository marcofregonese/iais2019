import numpy as np
import utils as Utils
import time
import logging
from proximity_sensor import Proximitysensor
from datetime import datetime
from go_to_goal_behavior import GoToGoalBehavior
from follow_wall_behavior import FollowWallBehavior
from avoid_obstacle_behavior import AvoidObstacleBehavior
from pick_up_object_behavior import PickUpObjectBehavior
from check_goal_pos_behavior import CheckGoalPosBehavior
from find_blobs_behavior import FindBlobsBehavior
from robotstatemachine import RobotStateMachine
import colors as Color


log = logging.getLogger('Robotcontroller')


class Robotcontroller:

    def __init__(self, robot):
        self.robot = robot
        self.proximity_sensor = Proximitysensor(self.robot.client_id, robot)
        self.now = time.time()

        self.calculated_pos = [0, 0, 0]     # Needed?
        self.direction = "cw"            # Helper for wall following
        self.velocity = 3                   # Robot Velocity
        self.omega = 0
        self.v_max = 7                  # maximum velocity
        self.t_max = 10                 # maximum time to move
        self.current_wheel_vels = self.robot.wheelVel(0, 0, 0)

        self.sensor_data = None         # proximity sensor data
        self.sensor_data = self.get_sensor_data()

        # State Machine and Behaviors
        self.state_machine = RobotStateMachine(self)
        self.gtg_behavior = GoToGoalBehavior(self)
        self.follow_wall_behavior = FollowWallBehavior(self)
        self.avoid_obstacle_behavior = AvoidObstacleBehavior(self)
        self.pick_up_object_behavior = PickUpObjectBehavior(self)
        self.check_goal_pos_behavior = CheckGoalPosBehavior(self)
        self.find_blobs_behavior = FindBlobsBehavior(self)
        self.current_behavior = self.gtg_behavior

        self.current_goal = None        # Location of current goal (blob)
        self.goal_reached_dist = 0.5    # Dist to stop in front of goal
        self.min_collision_dist = 0.35   # Dist to stop in front of obstacle
        self.too_close_dist = 0.2       # Dist when obstacle is too close

    def execute(self):
        while True:
            self._update_state()
            self.current_behavior.execute()
            self.move_robot(5, 0.1)

    def distance(self, target_x_y):
        pos_robot = self.robot.get_position()
        target_relative_to_robot = np.array(target_x_y) - np.array(pos_robot)
        # Distance in x direction
        dist_x = target_relative_to_robot[0]
        # Distance in x direction
        dist_y = target_relative_to_robot[1]
        # Absolute distance ( d = sqrt(x^2 + y^2))
        dist_abs = np.sqrt(np.square(dist_x) + np.square(dist_y))
        return dist_abs

    def get_direction_to(self, goal_x_y):
        return self.robot.get_goal_direction(goal_x_y)
        
    #inspired by: https://robotic-controls.com/learn/programming/pd-feedback-control-introduction
    def pd_controller(self, goal, kp, kd):
        output = 1
        dist = self.distance(goal)
        print(f"initial distance:{dist}")
        now = datetime.now().microsecond
        print(f"initial time microsecond:{now}")
        while(True):
            if( output > 0.01 and dist > 0.4 ):
                wv = output/0.04
                #self.robot.setVelocity(wv)
                self.move_robot(wv, 0.1)
            else:
                break
            last_dist = dist
            dist = self.distance(goal)
            print(f"distance:{dist}")
            lastTime = now
            now = datetime.now().microsecond
            print(f"time:{now}")
            speed = 1000000*(last_dist - dist ) / ( now - lastTime )
            print(f"speed:{speed}")
            output = kp*dist - kd*speed
            print(f"output:{output}")

    def pd_controller_horizontal(self, px_expected, px_actual):
        kp = 0.3
        kd = 0.4
        x_expected = px_expected[0]
        x_should = px_should[0]

    def pd_controller_vertical(self,px_expected, px_actual):
        kp = 0.3
        kd = 0.4
        y_expected = px_expected[1]
        y_should = px_should[1]

    def _update_state(self):
        log.debug("Updating states...")
        self._update_sensor_data()
        #  self._update_odometry()
        self.state_machine.update_state()

    def set_obj_as_goal(self, obj):
        log.info(f"Current goal: {obj}")
        obj_pos = obj[0][1]
        self.current_goal = Utils.vec2Dto3D(obj_pos)

    def look_for_object(self):
        H = self.robot.H2
        obj = self.robot.get_gp(H, Color.Red, "red")
        if not Utils.isEmpty(obj):
            self.set_obj_as_goal(obj)
            return True
        obj = self.robot.get_gp(H, Color.Blue, "Blue")
        if not Utils.isEmpty(obj):
            self.set_obj_as_goal(obj)
            return True
        return False

    def set_velocities(self, v, omega):
        self.velocity = v
        self.omega = omega

    def get_distance_to_current_goal(self):
        return self.distance(self.current_goal)

    def set_goal_pos(self, goal_pos):
        self.goal_pos = goal_pos

    def move_robot(self, v, t):
        wv = self.robot.wheelVel(v, 0, 0)
        self.robot.move(wv, t)

    def rotate_robot(self, v, t):
        wv = self.robot.wheelVel(0, 0, v)
        self.robot.move(wv, t)

    def get_sensor_data(self):
        self._update_sensor_data()
        return self.sensor_data

    def _update_sensor_data(self):
        log.debug("Updating sensor data...")
        self.proximity_sensor.update_sensor_data()

    def get_left_available_distance(self):
        sensor = self.robot.proximity_sensor
        return sensor.get_left_available_distance()

    def get_right_available_distance(self):
        sensor = self.robot.proximity_sensor
        return sensor.get_right_available_distance()

    def get_direction(self):
        return self.direction

    def get_sensor_distances_lcr(self):
        sensor = self.robot.proximity_sensor

        d_left = sensor.get_left_available_distance()
        d_right = sensor.get_right_available_distance()
        d_front = sensor.get_front_available_distance()

        return d_left, d_front, d_right

    def go_to_goal_pos(self, goal_pos):
        rot_angle = self.robot.get_goal_direction(goal_pos)
        dist_to_goal = self.distance(goal_pos)
        log.debug(f"rot_angle {rot_angle}")
        log.debug(f"dist_to_goal {dist_to_goal}")
        threshold_dist = 1.5
        threshold_rad = np.deg2rad(2)

        while(dist_to_goal > threshold_dist):
            log.debug(f"While: {dist_to_goal}")
            rot_angle = self.robot.get_goal_direction(goal_pos)

            while(abs(rot_angle) > threshold_rad):
                log.debug(f"rot_angle {rot_angle}")
                self.rotate_angle_controller(rot_angle)
                rot_angle = self.robot.get_goal_direction(goal_pos)

            dist_entry = dist_to_goal
            while(dist_to_goal > 0.5 * dist_entry
                  and dist_to_goal > threshold_dist):
                self.move_dist_controller(dist_to_goal)
                dist_to_goal = self.distance(goal_pos)
                log.debug(f"dist_to_goal {dist_to_goal}")

            dist_to_goal = self.robot.distance(goal_pos)

    def move_dist_sideways_controller(self, dist):
        """
            Moves sideways given distance with some kind of pd-controller 
            :param dist: distance to drive
        """
        R = self.robot.wheel_radius
        v = self.robot.velocity if dist > 0 else -self.robot.velocity
        t = dist / (v * R)
        dt = t if t < self.t_max else self.t_max
        wv = self.robot.wheelVel(0, v, 0)
        self.robot.move(wv, dt)

    def move_dist_controller(self, dist):
        """
            Moves straght given distance with some kind of pd-controller 
            :param dist: distance to drive
        """
        R = self.robot.wheel_radius
        v = self.robot.velocity if dist > 0 else -self.robot.velocity
        t = dist / (v * R)
        dt = t if t < self.t_max else self.t_max
        self.move_robot(v, dt*0.5)

    def rotate_angle_controller(self, angle):
        """
            Rotates given angle with some kind of pd-controller 
            :param angle: rotation angle
        """
        # Angle should be between 0 and 360
        sign = np.sign(angle)
        new_angle = abs(angle)
        while new_angle > 2 * np.pi:
            new_angle = new_angle - np.pi
        new_angle = sign * angle
        # Angle should be between -180 and 180
        if abs(angle) > np.pi:
            sign = np.sign(angle)
            angle = 2 * np.pi - abs(angle)
            angle = sign * angle * (-1)
        log.debug(f"Angle to rotate {np.rad2deg(angle)}")
        wv = self.robot.wheelVel(0, 0, self.velocity)
        w = self.robot.get_angluar_velocity(wv)
        w = w if w != 0 else 0.5

        t = abs(angle) / w
        t = t if t < self.t_max else self.t_max

        if(angle > 0):
            vel = -self.robot.velocity
        else:
            vel = self.robot.velocity
        self.rotate_robot(vel, t*0.7)

    def get_min_sensor_dist(self):
        min_dist, _ = self.proximity_sensor.get_angle_with_min_dist()
        return min_dist

    def get_angle_of_min_sensor_dist(self):
        """ 
        Returns angle of minimum sensor distance
        """
        _, angle = self.proximity_sensor.get_angle_with_min_dist()
        return angle

    def get_distance_at_angle(self, angle):
        a_max = angle + np.deg2rad(5)
        a_min = angle - np.deg2rad(5)
        dists =  self.proximity_sensor.get_distances_from_to_angle(a_min, a_max)
        return sum(dists)/len(dists)

    def _update_odometry(self):
        log.info("updating odometry...")
        old_time = self.now
        self.now = time.time()

        dt = self.now - old_time

        # calculate distance traveled from front left and front right wheel
        omega_fl = self.current_wheel_vels[1]
        omega_fr = self.current_wheel_vels[2]
        log.debug(f"w_l, w_r: {omega_fl}, {omega_fr}")
        # distance each wheel has traveled
        # https://link.springer.com/content/pdf/10.1007%2F978-3-319-62533-1.pdf
        # Page 72
        R = self.robot.wheel_radius
        B = self.robot.tread_width
        d_fl = R * omega_fl * dt
        d_fr = R * omega_fr * dt
        # for small angles equals length of arc theta = d_fl / r_fl
        d_theta = (d_fr - d_fl) / B
        # center is halfway between the wheels
        d_center = (d_fl + d_fr) / 2
        dx = (-1) * d_center * np.sin(d_theta)
        dy = d_center * np.cos(d_theta)

        # calculate new pose
        [old_x, old_y, old_theta] = self.calculated_pos
        new_x = old_x + dx
        new_y = old_y + dy
        new_theta = old_theta + d_theta
        self.calculated_pos = [new_x, new_y, new_theta]
        log.debug(f"Odometry Current pos: {self.calculated_pos}")
