import vrep
import numpy as np
import array
from vrepobject import Vrepobject
from PIL import Image       # I installed package "pillow"
import cv2
import logging

log = logging.getLogger('RgbSensor')


class RgbSensor(Vrepobject):

    def __init__(self, client_id, name):
        Vrepobject.__init__(self, client_id, name)
        self._def_op_mode = vrep.simx_opmode_oneshot_wait

        " First call to image "
        res, resolution, image = vrep.simxGetVisionSensorImage(
                self.client_id,
                self.handle,
                0,
                vrep.simx_opmode_streaming)
        self.persp_angle = np.deg2rad(120)

    def set_perspective_angle(self, angle):
        # angle of sensor
        self.persp_angle = angle

    def get_image(self):
        res, resolution, image = vrep.simxGetVisionSensorImage(
                self.client_id,
                self.handle,
                0,
                self._def_op_mode)
        Vrepobject.check_for_errors(res, "Sensor getImage")

        # transformation to byte array
        image_byte_array = np.asarray(array.array('b', image))
        # transformation to opencv2 image
        pil_image = Image.frombuffer(
                "RGB",
                (resolution[0], resolution[1]),
                image_byte_array,
                "raw", "RGB", 0, 1)

        # Correct colors: Opencv uses Color-Format BGR instead of RGB
        image = cv2.cvtColor(np.array(pil_image), cv2.COLOR_RGB2BGR)
        # Rotate Image because it is mirrored
        image = cv2.flip(image, 0)
        return image

    def track_color(self, image, color):
        # https://docs.opencv.org/3.4.0/df/d9d/tutorial_py_colorspaces.html

        #  color = color[::-1]              # convert color from RGB2BGR
        # define range of specified color
        color = np.uint8([[color]])     # color in rgb [r, g ,b]
        hsv_color = cv2.cvtColor(color, cv2.COLOR_BGR2HSV)
        hue_color = hsv_color[0][0][0]
        lower_color = np.array([hue_color-10, 50, 50])
        upper_color = np.array([hue_color+10, 255, 255])

        # Convert from BGR to HSV color-space
        #  log.debug(f"{image}")
        hsv_image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2HSV)

        # Threshold the image to get only specified color
        mask = cv2.inRange(hsv_image, lower_color, upper_color)

        # Bitwise-AND mask and original image
        result = cv2.bitwise_and(image, image, mask=mask)
        #  self.show_image(result)

        return result

    def get_contour(self, img):
        """
        r1 is a list of arrays for each object detected
        with the boundary coordinates
        """
        imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(imgray, 30, 255, 0)
        r1, r2 = cv2.findContours(thresh,
                                  cv2.RETR_TREE,
                                  cv2.CHAIN_APPROX_SIMPLE)
        return r1

    def show_image(self, cv_image):
        if(len(cv_image) > 1):
            cv2.imshow('Image', cv_image)
            cv2.waitKey(0)

    def get_sensor_data_as_matrix(self):
        res, det_state, auxData = vrep.simxReadVisionSensor(
                self.client_id, self.handle,
                vrep.simx_opmode_buffer)

        # transform auxData to 342x4 matrix
        width = int(auxData[1][0])
        length = int(auxData[1][1])
        sensor_data_matrix = np.reshape(auxData[1][2:], [width*length, 4])
        return sensor_data_matrix

    def get_imgByColor(self, color):
        img = self.get_image()
        tracked_img = self.track_color(img, color)
        flip = cv2.flip(tracked_img, 0)
        return flip

    def get_imgObjBottomCoordinates(self, color):
        image = self.get_imgByColor(color)
        contours = self.get_contour(image)
        log.debug(f"contours:\n{contours}")
        bottomCoordinates = []
        for i in contours:
            min_y = 511
            for j in i:
                if(min_y > j[0][1]):
                    min_y = j[0][1]
                    x = j[0][0]
            coordinates = [x, min_y]
            log.debug(x, min_y)
            bottomCoordinates.append(coordinates)
        return bottomCoordinates

    def get_imgObjTopCoordinates(self, color):
        image = self.get_imgByColor(color)
        contours = self.get_contour(image)
        log.debug(f"contours:\n{contours}")
        topCoordinates = []
        for i in contours:
            max_y = 0
            for j in i:
                if(max_y < j[0][1]):
                    max_y = j[0][1]
                    x = j[0][0]
            coordinates = [x, max_y]
            log.debug(x, max_y)
            topCoordinates.append(coordinates)
        return topCoordinates

    def isObjectHorizontalCentered(self, img):
        height = len(img)
        width = len(img[0])
        horizontal_center = width/2
        log.debug(f"Width {width}, Height {height}\n")

        # Get first and last indices of array, where color occurs
        # so we know where to look for it... with the perspective angle (45deg)
        # we can conclude how much to rotate...
        idx_color_first = None
        idx_color_last = None

        for i_row, row in enumerate(img):
            for i_col, px in enumerate(row):
                if px[0] != 0 or px[1] != 0 or px[2] != 0:
                    if idx_color_first is None:
                        idx_color_first = [i_row, i_col]
                    idx_color_last = [i_row, i_col]
        log.debug(idx_color_first)
        log.debug(idx_color_last)
        obj_size = np.array(idx_color_last) - np.array(idx_color_first)
        log.debug(f"Object width: {obj_size[0]}, object height {obj_size[1]}")
        half_obj_width = obj_size[0]/2
        should_be_center = idx_color_first[1] + half_obj_width
        diff_center_object = horizontal_center - should_be_center
        log.debug(f"Center: {horizontal_center}, Should be center: \
              {should_be_center}")
        if(abs(diff_center_object) < 10):
            log.debug("Object is centered")
        return diff_center_object

    def get_chessboardCorners(self):
        """
        to access nth element coordinates add always [0]
            e.g.: ret = get_chessboardCorner()  ret[nth][0]
        """
        # 5 x 4 chessboard with 3 x 4 intersection points
        pattern_size = (4, 3)
        img = self.get_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # flip() in order to match the pixels with world coordinate system
        flip = cv2.flip(gray, 0)
        ret, corners = cv2.findChessboardCorners(flip, pattern_size)
        imgpoints = []
        imgpoints.append(corners)
        # self.show_image(img)
        return imgpoints[0]
