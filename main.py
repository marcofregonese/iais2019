# Introduction to Intelligent and Autonomus Systems, UIBK, 2017
# contact:senka.krivic@uibk.ac.at
import numpy as np
import vrep
from robot import Robot
from vrepobject import Vrepobject
from distbug import DistBug


def main():
    print('Program started')
    vrep.simxFinish(-1)     # just in case, close all opened connections
    clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 2000, 5)
    if clientID != -1:
        print('Connected to remote API server')

        # emptyBuff = bytearray()

        # Start the simulation:
        vrep.simxStartSimulation(clientID, vrep.simx_opmode_oneshot_wait)

        goal = Vrepobject(clientID, 'Goal')
        rob = Robot(clientID)
        rob.setVelocity(8)
        dist_bug = DistBug(rob)
        rob.findGoal(dist_bug, goal)

        # Stop simulation:
        vrep.simxStopSimulation(clientID, vrep.simx_opmode_oneshot_wait)

        # Now close the connection to V-REP:
        vrep.simxFinish(clientID)
    else:
        print('Failed connecting to remote API server')
    print('Program ended')


if __name__ == "__main__":
    main()
