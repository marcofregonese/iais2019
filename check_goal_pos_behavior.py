import numpy as np
import logging
log = logging.getLogger('CheckGoalPosBehavior')


class CheckGoalPosBehavior():
    def __init__(self, controller):
        self.controller = controller
        self.robot = self.controller.robot

    def execute(self):
        ctrl = self.controller

        log.debug("Check goal position")
        if ctrl.current_goal is None:
            return
        else:
            goal = ctrl.current_goal
            angle_to_goal = ctrl.get_direction_to(goal)
            log.debug(f"Rotate camera to goal")
            log.debug(f"Angle to goal {np.round(np.rad2deg(angle_to_goal), 2)}")
            angle_camera = np.deg2rad(-110)
            low = angle_camera - np.deg2rad(4)
            high = angle_camera + np.deg2rad(4)
            while(angle_to_goal > high or angle_to_goal < low):
                angle = (angle_camera - angle_to_goal) * (-1)
                log.debug(f"Rotate {np.round(np.rad2deg(angle), 2)}")
                ctrl.rotate_angle_controller(angle)
                angle_to_goal = ctrl.get_direction_to(goal)
                log.debug(f"""{np.round(np.rad2deg(angle_to_goal), 2)} <> 
                    {np.round(np.rad2deg(angle_camera), 2)}""")
            ctrl.look_for_object()

