import logging
log = logging.getLogger('RobotStateMachine')


class RobotStateMachine:
    def __init__(self, controller):
        self.controller = controller
        self.initial_state = RobotStates.GO_TO_GOAL
        self.current_state = self.initial_state
        self.goal_pos_checking_counter = 0
        self.do_recheck_goal_position = False
        self.dist_to_goal = 0

    def update_state(self):
        new_state = self.current_state
        log.info('Updating States...')

        if self.goal_exists():
            if self.is_goal_reached():
                new_state = RobotStates.PICK_UP_OBJECT
            elif self.is_goal_pos_recheck_needed():
                new_state = RobotStates.CHECK_GOAL_POSITION
            elif self.is_obstacle_found():
                new_state = RobotStates.AVOID_OBSTACLES
            else:
                new_state = RobotStates.GO_TO_GOAL
        else:
            new_state = RobotStates.FIND_BLOBS
        self.current_state = new_state
        log.debug(f"Current state {self.current_state}")
        self.prepare_state()

    def prepare_state(self):
        ctrl = self.controller
        state = self.current_state
        if state == RobotStates.FIND_BLOBS:
            log.info("FIND_BLOBS")
            ctrl.current_behavior = ctrl.find_blobs_behavior
        elif state == RobotStates.PICK_UP_OBJECT:
            log.info("GOAL REACHED - PICK UP OBJECT")
            ctrl.current_behavior = ctrl.pick_up_object_behavior
        elif state == RobotStates.AVOID_OBSTACLES:
            log.info("AVOID_OBSTACLES")
            ctrl.current_behavior = ctrl.avoid_obstacle_behavior
        elif state == RobotStates.GO_TO_GOAL:
            log.info("GO_TO_GOAL")
            ctrl.current_behavior = ctrl.gtg_behavior
        elif state == RobotStates.CHECK_GOAL_POSITION:
            log.info("CHECK_GOAL_POSITION")
            ctrl.current_behavior = ctrl.check_goal_pos_behavior
            self.goal_pos_checking_counter = 0
            self.do_recheck_goal_position = False
        else:
            log.error("Undefiend State")
            exit(0)

    def goal_exists(self):
        return not self.no_goal_exists()

    def no_goal_exists(self):
        return self.controller.current_goal is None

    def is_goal_reached(self):
        dist_to_goal = self.controller.get_distance_to_current_goal()
        if dist_to_goal > 1.3:
            self.do_recheck_goal_position = True
        log.debug(f"Dist to goal: {dist_to_goal}")
        return dist_to_goal < self.controller.goal_reached_dist

    def is_obstacle_found(self):
        min_dist = self.controller.get_min_sensor_dist()
        log.debug(f"Min sensor dist: {min_dist}")
        return min_dist <= self.controller.min_collision_dist

    def is_obstacle_too_close(self):
        min_dist = self.controller.get_min_sensor_dist()
        return min_dist < self.controller.too_close_dist

    def is_goal_pos_recheck_needed(self):
        if self.do_recheck_goal_position:
            self.goal_pos_checking_counter += 1
        result = (self.do_recheck_goal_position and
                  self.goal_pos_checking_counter > 2)
        log.debug(f"Is_goal_pos_recheck_needed: {result}")
        log.debug(f"""{self.do_recheck_goal_position} and 
                  {self.goal_pos_checking_counter}""")
        return result


class RobotStates:
    PICK_UP_OBJECT = 0
    GO_TO_GOAL = 1
    CHECK_GOAL_POSITION = 2
    AVOID_OBSTACLES = 3
    FIND_BLOBS = 4
