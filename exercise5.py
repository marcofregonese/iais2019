# Introduction to Intelligent and Autonomus Systems, UIBK, 2017
# contact:senka.krivic@uibk.ac.at
import numpy as np
import vrep
from robot import Robot
from vrepobject import Vrepobject
from distbug import DistBug


def main():
    print('Program started')
    vrep.simxFinish(-1)     # just in case, close all opened connections
    clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 2000, 5)
    if clientID != -1:
        print('Connected to remote API server')

        # emptyBuff = bytearray()

        # Start the simulation:
        vrep.simxStartSimulation(clientID, vrep.simx_opmode_oneshot_wait)

        rob = Robot(clientID)
        #move the robot to visualize the chessboard
        rob.setVelocity(5)
        rob.sidewaysMeters(-0.3)
        #rob.rotateDegrees(10)
        rob.forwardMeters(0.1)

        corners = rob.get_chessboardCorners()
        rob.homography(corners)
        rob.homographyCV2(corners)

        # Stop simulation:
        vrep.simxStopSimulation(clientID, vrep.simx_opmode_oneshot_wait)

        # Now close the connection to V-REP:
        vrep.simxFinish(clientID)
    else:
        print('Failed connecting to remote API server')
    print('Program ended')


if __name__ == "__main__":
    main()
