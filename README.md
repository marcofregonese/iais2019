## Introduction to Autonomous and Intelligent Systems 2019

### Exercise 5

To get a clear explanation of how to build the matrix H(homography matrix) read point 4.1 starting at page 88(manual, link below).
The Direct Linear Transformation (DLT) algorithm to get H is at page 91(same manual).

http://cvrs.whu.edu.cn/downloads/ebooks/Multiple%20View%20Geometry%20in%20Computer%20Vision%20(Second%20Edition).pdf

Marco's log: 

1. In exercise5.py I renamed locate_image() into get_chessBoradCorners(), added greyscale and cv2.flip() in order to make the chessboard corners pixel coordinates match the world system coordinates
2. I implemented function get_egocentric_transformation_matrix() in order to transfrom the ground truth values of inner corners in the world frame to the robot egocentric frame
3. I implemented transformGroundPointToSensorEgoCentricFrame(x,y)
4. I implemented getAi(X, x) and assembleAs(A, Ai) to get matrix A.
5. I implemented the final homography function but the result doesn't match with the cv2.findHomography()!

### Examination 2

Marco's log:

1. Implemented functions in sensor.py and robot.py to get the bottom coordinates of the objects detected by colors
2. Problem with homography values solved: introduced get_UNDOegocentricTranformationMatrix()
3. Adjusted values in cv2.trheshold() to detect red and blue colors
4. added code in exam2.py to get final results as requested 

test
