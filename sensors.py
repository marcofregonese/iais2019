import vrep
import numpy as np
import array
import itertools
from vrepobject import Vrepobject
from PIL import Image       # I installed package "pillow"
import cv2


class Sensor(Vrepobject):

    def __init__(self, client_id, name):
        Vrepobject.__init__(self, client_id, name)
        self._def_op_mode = vrep.simx_opmode_oneshot_wait

        " First call to read Vision sensor "
        res, det_state, auxData = vrep.simxReadVisionSensor(
                self.client_id, self.handle,
                vrep.simx_opmode_streaming)
        " First call to image "
        res, resolution, image = vrep.simxGetVisionSensorImage(
                self.client_id,
                self.handle,
                0,
                vrep.simx_opmode_streaming)

    def set_perspective_angle(self, angle):
        # angle of sensor
        self.persp_angle = angle

    def read(self):
        code, state, aux_packets = vrep.simxReadVisionSensor(
            self._id, self._handle, self._def_op_mode)
        return state, state, aux_packets

    def get_image(self):
        res, resolution, image = vrep.simxGetVisionSensorImage(
                self.client_id,
                self.handle,
                0,
                self._def_op_mode)
        Vrepobject.check_for_errors(res, "Sensor getImage")

        # transformation to byte array
        image_byte_array = np.asarray(array.array('b', image))
        # transformation to opencv2 image
        pil_image = Image.frombuffer(
                "RGB",
                (resolution[0], resolution[1]),
                image_byte_array,
                "raw", "RGB", 0, 1)
        #  print(f"Image:\n\tSize: {pil_image.size}")
        #  print(f"\tColour Palette: {pil_image.palette}")

        # image_buffer.show() # show image with "PIL"

        # Correct colors: Opencv uses Color-Format BGR instead of RGB
        image = cv2.cvtColor(np.array(pil_image), cv2.COLOR_RGB2BGR)
        # Rotate Image because it is mirrored
        image = cv2.flip(image, 0)
        #  cv2.imshow('Camera', image)     # show image with opencv
        #  cv2.waitKey(0)                  # waits for keypress when image open
        return image

    def track_color(self, image, color):
        # https://docs.opencv.org/3.4.0/df/d9d/tutorial_py_colorspaces.html

        color = color[::-1]              # convert color from RGB2BGR
        # define range of specified color
        color = np.uint8([[color]])     # color in rgb [r, g ,b]
        hsv_color = cv2.cvtColor(color, cv2.COLOR_RGB2HSV)
        hue_color = hsv_color[0][0][0]
        lower_color = np.array([hue_color-10, 100, 100])
        upper_color = np.array([hue_color+10, 255, 255])
        #print(f"lower : {lower_color}, upper: {upper_color}")

        # Convert from BGR to HSV color-space
        hsv_image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2HSV)

        # Threshold the image to get only specified color
        mask = cv2.inRange(hsv_image, lower_color, upper_color)

        # Bitwise-AND mask and original image
        result = cv2.bitwise_and(image, image, mask=mask)

        #  if(all(x == 0) for x in itertools.chain(result)):
        #      print("Object not found in Image")
        #      return []
        #  cv2.imshow('Mask', mask)

        return result

    #r1 is a list of arrays for each object detected with the boundary coordinates
    def get_contour(self, img):
        imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(imgray, 30, 255, 0)
        r1, r2 = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        return r1

    def show_image(self, cv_image):
        if(len(cv_image) > 1):
            cv2.imshow('Image', cv_image)
            cv2.waitKey(0)

    def get_sensor_data_relative_to(self, transformation_frame):
        sensor_data_matrix = self.get_sensor_data_as_matrix()

        # get pos of sensor relative to transformation_frame
        sensor_pos = self.get_position_relative_to(transformation_frame)
        # get orientation of sensor relative to transformation_frame
        sensor_orient = self.get_orientation_relative_to(transformation_frame)
        
        #  print("pos, orient of sensor", sensor_pos, sensor_orient)

        # Calculate Euler-Angles
        # according to docs/globalLocalCoordinationTransformation.pdf
        alpha, beta, gamma = sensor_orient[0], sensor_orient[1], sensor_orient[2]

        # Transformation matrices for x-, y- and z-Direction
        Rot_x = np.array([
            [1,             0,                  0               ],
            [0,             np.cos(alpha),      -np.sin(alpha)  ],
            [0,             np.sin(alpha),      np.cos(alpha)   ]])

        Rot_y = np.array([
            [np.cos(beta),  0,                  np.sin(beta)    ],
            [0,             1,                  0               ],
            [-np.sin(beta), 0,                  np.cos(beta)    ]])

        Rot_z = np.array([
            [np.cos(gamma),  -np.sin(gamma),    0               ],
            [np.sin(gamma),  np.cos(gamma),     0               ],
            [0,             0,                  1               ]])

        # The rotation matrix is the dot product of the three elementary
        # rotations
        Rot = np.dot(Rot_x, Rot_y, Rot_z)

        for i in range(0, len(sensor_data_matrix)):
            # Current Data Entry we work on (x, y, distance)
            curr_data = sensor_data_matrix[i, 0:3]
            # Transform matrix-entry into 3x1 vector
            sensor_data_vec = np.reshape(curr_data, [3, 1])
            # change orientation of sensordata to match youbot-frame,
            # therefore position vector is multiplied by the rotation matrix
            pos_rotated = np.dot(Rot, sensor_data_vec)
            # Transform to 1x3 matrix
            pos_rotated = np.reshape(pos_rotated, [1,3])
            # next the translation offset is added to generate a description of
            # the position and orientation of the sensor
            transformed_pos = pos_rotated + sensor_pos
            transformed_pos[0][0] *= -1
            transformed_pos[0][1] *= -1
            # finally write new position back to sensor_data_matrix
            sensor_data_matrix[i, 0:3] = transformed_pos

        return sensor_data_matrix

    def get_sensor_data_as_matrix(self):
        res, det_state, auxData = vrep.simxReadVisionSensor(
                self.client_id, self.handle,
                vrep.simx_opmode_buffer)

        # transform auxData to 342x4 matrix
        width = int(auxData[1][0])
        length = int(auxData[1][1])
        sensor_data_matrix = np.reshape(auxData[1][2:], [width*length, 4])
        return sensor_data_matrix

