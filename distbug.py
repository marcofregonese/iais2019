import numpy as np
import time

class DistBug:

    state = "GO TO GOAL"
    direction = " "
    # rotation direction cw ccw
    cw = -1
    ccw = 1
    # curr_dist = distance from the current location to the target
    curr_dist = 100
    # hit_dist = distance from the last hit point H to the target
    hit_dist = 100
    # step = a constant that will be the minimal improvement in the distance to
    # the target between hit points.
    step = 1
    # best_dist = the up-to-date shortest distance to the target since the last
    # hit point
    # Initialize as: best_dist = (hit_dist - step).
    best_dist = hit_dist - step
    # free_dist = distance in freespace from the current location to the nearest
    # obstacle in the direction to the target
    free_dist = 0.1
    # min_collision_dist = The minimum distance that the robot can have from an
    # obstacle
    min_collision_dist = 0.6
    # left/right available dist to choose direction to follow obstacle
    left = 0
    right = 0
    hit = True
    sensor_data = []

    def __init__(self, robot_handler):
        self.robot = robot_handler

    def findGoal(self, goal_handler):
        print("Looking for the goal...")
        self.goal = goal_handler
        self.state_machine()

    def is_target_reached(self):
        return abs(self.robot.get_goal_distance()) < 0.5
    
    def is_obstacle_reached(self, front_dist):
        return front_dist <= self.min_collision_dist + 0.2
        #  self.get_data_with_min_distance()[3] < 0.9

    def state_machine(self):
        print("STATE MACHINE")
        self.hit = False
        self.robot.rotate_towards_goal()
        self.sensor_data = self.robot.get_sensors_data()
        front_dist = self.robot.get_front_available_distance(self.sensor_data)
        if front_dist < 0.2:
            print("Too close to obstacle")
            self.robot.forwardMeters(-0.3)
        if self.is_target_reached():
            print("GOAL FOUND")
            return
        elif self.is_obstacle_reached(front_dist):
            self.state == "FOLLOW OBSTACLE"
            self.choose_direction()
            self.follow_wall()
        else:
            self.state == "GO TO GOAL"
            self.go_to_goal()
        self.state_machine()

    def get_data_with_min_distance(self):
        index_min_dist = np.argmin(self.sensor_data[:, 3])
        return self.sensor_data[index_min_dist]

    def go_to_goal(self):
        print("GO TO GOAL behaviour")
        # for not hanging in obstacle, increasing fault tolerance
        d1, d2 = self.robot.get_front_distances(self.sensor_data)
        if d1 < 0.2 or d2 < 0.2:
            print("Too close to obstacle")
            self.robot.forwardMeters(-0.3)

        self.robot.rotate_towards_goal()
        goal_dist = self.robot.get_goal_distance() 
        self.sensor_data = self.robot.get_sensors_data()
        front_dist = self.robot.get_front_available_distance(self.sensor_data)
        print("goaldist:", goal_dist, "front_dist", front_dist)
        if goal_dist < front_dist:
            self.robot.forwardMeters(goal_dist)
        else:
            self.robot.forwardMeters(front_dist - self.min_collision_dist)
    
    # work in progress
    #  def follow_obstacle(self):
    #      # Current distance to target
    #      self.curr_dist = self.robot.get_goal_distance()
    #      # hit distance
    #      if not self.hit:
    #          self.hit = True
    #          self.hit_dist = self.curr_dist
    #          self.best_dist = self.hit_dist
    #      # Sensing the distance in freespace in direction to target
    #      self.robot.rotate_towards_goal()
    #      self.free_dist = self.robot.get_front_free_distance()
    #
    #      # Recording minimal distance to Target
    #      if self.best_dist < self.curr_dist:
    #          self.best_dist = self.curr_dist
    #
    #      self.choose_direction()
    #      if self.direction == "RIGHT":
    #          self.go_along_left_obstacle()
    #      elif self.direction == "LEFT":
    #          self.go_along_right_obstacle()
    #      self.state = "FOLLOW OBSTACLE"
    #      print("FOLLOW OBSATCLE behaviour")

    def range_leaving_condition_holds(self):
        return self.curr_dist - self.free_dist <= self.best_dist - self.step

    def is_target_visible(self):
        return self.curr_dist - self.free_dist < 0
       
    def follow_wall(self, direction="cw"):
        # mostly with help of 
        # http://students.iitk.ac.in/robocon/docs/doku.php?id=robocon16:programming:wall_following
        # The stop-and-turn-part

        print("Follow wall")
        # check environment
        self.sensor_data = self.robot.get_sensors_data()
        
        # We have an a certain angle left and rigth of the robot, in which we
        # sensor left and right. The areas are bounded by 2 sensor-data entries
        # (look at the link)
        if self.direction == "RIGHT":
            d1, d2 = self.robot.get_left_distances(self.sensor_data)
        else:
            d1, d2 = self.robot.get_right_distances(self.sensor_data)
            
        d_front = self.robot.get_front_available_distance(self.sensor_data)
        
        # if wall in front
        if(d_front < self.min_collision_dist - 0.2):
            print("Wall in front")
            # rotation direction depends whether we follow clockwise or
            # counterclockwise ( cw (clockwise)/ccw (counterclockwise))
            # if dir == RIGHT => wall is left of robot => wall in front (corner)
            # => turn cw
            deg = self.cw*90 if self.direction == "RIGHT"  else self.ccw*90
            self.robot.rotateDegrees(deg)
        # if corner (obstacle ends)
        if(d1 > 1 and d2 > 1):
            print("End of wall")
            self.robot.forwardMeters(0.4)
            deg = self.ccw*90 if self.direction == "RIGHT" else self.cw*90
            self.robot.rotateDegrees(deg)
            return

        # adjust direction if necessary
        self.adjust_direction()

        # go forward a bit
        self.robot.forwardMeters(0.2)

        # recursion produces a stackoverflow, if the followed obstacle is too
        # big (works most of the time)
        self.follow_wall()

    # work in progress
    #  def go_along_left_obstacle(self, direction="left"):
    #      print("GO ALONG LEFT OBSTACLE behaviour")
    #      self.robot.forwardMeters(0.1)
    #      distance = 0.9
    #      signum = 1 if (direction == "left") else -1
    #      front_distance = 1
    #      while(distance < 1 and front_distance > self.min_collision_dist):
    #          self.robot.forwardMeters(1)
    #          if direction == "left":
    #              self.adjust_direction_right_obstacle()
    #              distance = self.robot.get_left_available_distance()
    #          else:
    #              self.adjust_direction_left_obstacle()
    #              distance = self.robot.get_right_available_distance()
    #
    #          front_distance = self.robot.get_front_available_distance()
    #          print(direction, "_dist", distance, " front_dist", front_distance)
    #
    #      if(distance > 1.0):
    #          print("Opening")
    #          self.robot.forwardMeters(0.2)
    #          self.robot.rotateDegrees(90*signum)
    #      elif(front_distance < self.min_collision_dist):
    #          print("Obstacle in Front")
    #          if front_distance < 0.2:
    #              self.robot.forwardMeters(-0.5)
    #          self.robot.rotateDegrees(signum*90)

    #  def go_along_right_obstacle(self):
    #      self.go_along_left_obstacle("right")

    def adjust_direction(s):
        # depends on direction choosed by choose_direction
        # counterclockwise (ccw) vs clockwise (cw)
        print("Adjust direction")
        if s.direction == "RIGHT":
            # if dir == right => obstacle left of robot
            distance = s.robot.get_left_available_distance(s.sensor_data)
        else:
            distance = s.robot.get_right_available_distance(s.sensor_data)

        print(f"Distance to wall: {distance}")
        if (distance < s.min_collision_dist - 0.2):
            print("Too close, rotate away from obstacle")
            # if dir == RIGHT => obstacle left of robot => turn away = turn cw
            direct = s.cw if s.direction == "RIGHT" else s.ccw
            s.robot.rotateDegrees(direct*5)
        if (distance > s.min_collision_dist + 0.2):
            print("Too far, rotate towards obstacle")
            direct = s.ccw if s.direction == "RIGHT" else s.cw
            s.robot.rotateDegrees(direct*5)
        return

    def choose_direction(self):
        print("CHOOSE DIRECTION behaviour")
        self.left = self.robot.get_left_available_distance(self.sensor_data)
        self.right = self.robot.get_right_available_distance(self.sensor_data)
        direction = self.left - self.right
        # print("left", self.left, "right", self.right, "direction", direction)
        if direction < 0:
            print("TURN RIGHT")
            self.direction = "RIGHT"
            self.rotate_parallel_to_obstacle_right()
        else:
            print("TURN LEFT")
            self.direction = "LEFT"
            self.rotate_parallel_to_obstacle_left()

    def rotate_parallel_to_obstacle_left(self):
        # rotate parallel
        min_dist_datum = self.get_data_with_min_distance()
        min_dist_x = min_dist_datum[0]
        min_dist_y = min_dist_datum[1]
        angle = np.arctan(abs(min_dist_x)/abs(min_dist_y))
        self.robot.rotateRadians(-angle)

    def rotate_parallel_to_obstacle_right(self):
        # rotate parallel
        min_dist_datum = self.get_data_with_min_distance()
        min_dist_x = min_dist_datum[0]
        min_dist_y = min_dist_datum[1]
        angle = np.arctan(abs(min_dist_y)/abs(min_dist_x))
        self.robot.rotateRadians(-angle)

    #  def rotate_right_parallel_to_obstacle(self):
    #      hypotenuse = self.robot.get_front_available_distance()
    #      min_dist_datum = self.get_data_with_min_distance()
    #      opposite = min_dist_datum[3]
    #      angle = np.arcsin(opposite/(hypotenuse+0.15))
    #      self.robot.rotateRadians(-angle)
    #
    #  def rotate_left_parallel_to_obstacle(self):
    #      #  self.rotate_parallel_to_obstacle("LEFT")
    #      hypotenuse = self.robot.get_front_available_distance()
    #      min_dist_datum = self.get_data_with_min_distance()
    #      opposite = min_dist_datum[3]
    #      angle = np.arcsin(opposite/(hypotenuse+0.15))
    #      self.robot.rotateRadians(angle)
    #
