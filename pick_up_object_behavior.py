import numpy as np
import logging
import time

log = logging.getLogger('PickUpObjectBehavior')


class PickUpObjectBehavior:
    def __init__(self, controller):
        self.controller = controller
        self.robot = self.controller.robot
        self.arm = self.robot.arm
        self.grab_angle = np.deg2rad(-90)

    def execute(self):
        ctrl = self.controller
        goal_pos = ctrl.current_goal
        rot_angle = ctrl.get_direction_to(goal_pos)

        dist_to_goal = ctrl.get_distance_to_current_goal()
        angle_to_goal = ctrl.get_direction_to(goal_pos)
        #  log.debug(f"Dist2Goal: {dist_to_goal}")
        #  log.debug(f"Angle2Goal: {np.rad2deg(angle_to_goal)}")
        #  log.debug(f"""Angle2Goal: {np.rad2deg(angle_to_goal)} >
        #            {np.rad2deg(self.grab_angle + np.deg2rad(3))}""")

        grab_dist = 0.35

        #  while self.is_not_grab_angle(angle_to_goal):
        #      rot_angle = (self.grab_angle - angle_to_goal) * (-1)
        #      ctrl.rotate_angle_controller(rot_angle)
        #      angle_to_goal = ctrl.get_direction_to(goal_pos)
        #      log.debug(f"""Angle2Goal: {np.rad2deg(angle_to_goal)} >
        #                {np.rad2deg(self.grab_angle + np.deg2rad(3))}""")
        #
        #  while dist_to_goal > grab_dist:
        #      move_dist = -0.03
        #      ctrl.move_dist_sideways_controller(move_dist)
        #      dist_to_goal = ctrl.get_distance_to_current_goal()
        #      log.debug(f"Dist2Goal: {dist_to_goal}")

        Blue = [84, 68, 255]
        blob_pixels = \
            self.controller.robot.rgb_sensor.get_imgObjTopCoordinates(Blue)

        log.debug(f"pixels {blob_pixels}")

        self.arm.pick_up_obj(goal_pos)
        self.controller.current_goal = None
        #  raise ObjectPickedUpException()

    def is_not_grab_angle(self, angle):
        is_angle = angle
        should_angle = self.grab_angle
        should_max = should_angle + np.deg2rad(2)
        should_min = should_angle - np.deg2rad(2)
        result = ((is_angle > should_max) or
                  (is_angle < should_min))
        log.debug(f"IsNotGrabAngle: {result}")
        return result


class ObjectPickedUpException(Exception):
    pass
