# Introduction to Intelligent and Autonomus Systems, UIBK, 2017
# contact:senka.krivic@uibk.ac.at
import numpy as np
import traceback
import vrep
from robot import Robot
from vrepobject import Vrepobject
from distbug import DistBug

##########
#        #   
# EXAM 2 #
#        #
##########
'''
     Using color-based object detection methods and without changing robot location (rotation in-place is allowed), compute and print the ordered list of colored objects (from left to right or right to left). The following information is required: color, position of the bottom in the image frame
    Using homography, compute the (2D) object positions in the world frame. The goal of the robot is to go between the 3 red objects; thus, deduce from vision an admissible position to reach.
    Display coordinates of detected objects both in the camera frame and in the world frame.
    Using your favorite method, drive the robot to the goal location WITHOUT hitting the objects!
    Bonus point: use the odometry estimation rather than the simulator ground truth for the robot position

Minimum expected outputs:

    positions of objects in both frames (printed out)
    demonstration of the robot reaching the correct location (simulation)

Hints:

    The chessboard is at [0, 0.5, 0] and is the same than in previous exercise.
    Detect bottom points of objects and calculate their positions using your homography matrix
    Your robot needs to be able to recognize objects of the following colors: green, red, blue, orange and yellow
    Depending on your navigation strategy, you can use the method to find the goal position to find intermediate goals.
'''

def main():
    print('Program started')
    vrep.simxFinish(-1)     # just in case, close all opened connections
    clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 2000, 5)
    if clientID != -1:
        print('Connected to remote API server')

        # emptyBuff = bytearray()

        # Start the simulation:
        vrep.simxStartSimulation(clientID, vrep.simx_opmode_oneshot_wait)

        rob = Robot(clientID)
        #move the robot to visualize the chessboard
        rob.setVelocity(5)
        colors = []
        green = [0,255,0]
        blue = [0,0,255]
        red = [255,0,0]
        orange = [255,128,0]
        yellow = [255,255,0]
        #  colors.append('green', green)
        #  colors.append('blue', blue)
        #  colors.append('red', red)
        #  colors.append('orange', orange)
        #  colors.append('yellow', yellow)
        
        res = []
        ret0 = rob.get_imgObjBottomCoordinates(orange)
        res.append(("orange", ret0))
        ret1 = rob.get_imgObjBottomCoordinates(yellow)
        res.append(("yellow", ret1))
        ret2 = rob.get_imgObjBottomCoordinates(green)
        res.append(("green",ret2))
        rob.rotateDegrees(-45)
        ret3 = rob.get_imgObjBottomCoordinates(yellow)
        res.append(("yellow",ret3))
        ret4 = rob.get_imgObjBottomCoordinates(blue)
        res.append(("blue",ret4))
        ret5 = rob.get_imgObjBottomCoordinates(red)
        res.append(("red",ret5))
        rob.rotateDegrees(-20)
        ret6 = rob.get_imgObjBottomCoordinates(green)
        res.append(("green",ret6))
        rob.rotateDegrees(-20)
        ret7 = rob.get_imgObjBottomCoordinates(orange)
        res.append(("orange",ret7))
        print(f"result img coordinates:\n{res}")
        
        #code to get homographies
        rob.rotateDegrees(85)
         
        rob.sidewaysMeters(-0.3)
        rob.forwardMeters(0.17)

        res = []
        corners = rob.get_chessboardCorners()
        if(corners is not None):
            print(f"chessboard corners:\n{corners}")
            H2 = rob.homographyCV2(corners)
            #H = rob.homography(corners)
            r0 = rob.get_gp(H2, orange, "orange")
            res.append(r0)
            r1 = rob.get_gp(H2, green, "green")
            res.append(r1)
            r2 = rob.get_gp(H2, yellow, "yellow")
            res.append(r2)

            rob.rotateDegrees(-40)

            r3 = rob.get_gp(H2, blue, "blue")
            res.append(r3)
            r4 = rob.get_gp(H2, red, "red")
            res.append(r4)
            r5 = rob.get_gp(H2, yellow, "yellow")
            res.append(r5)

            rob.rotateDegrees(-40)

            r6 = rob.get_gp(H2, green, "green")
            res.append(r6)
            r7 = rob.get_gp(H2, orange, "orange")
            res.append(r7)
        else:
            print(f"corners is None")

        print(res)

        # Stop simulation:
        vrep.simxStopSimulation(clientID, vrep.simx_opmode_oneshot_wait)

        # Now close the connection to V-REP:
        vrep.simxFinish(clientID)
    else:
        print('Failed connecting to remote API server')
    print('Program ended')


if __name__ == "__main__":
    main()
