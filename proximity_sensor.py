from sensors import Sensor
import vrep
from vrepobject import Vrepobject
import numpy as np
import matplotlib.pyplot as plot
import logging
log = logging.getLogger('Proximitysensor')

class Proximitysensor:
    def __init__(self, client_id, robot):
        self.client_id = client_id
        self.hokuyo1 = Sensor(self.client_id, 'fastHokuyo_sensor1')
        self.hokuyo2 = Sensor(self.client_id, 'fastHokuyo_sensor2')
        self.robot_handler = robot.get_handle()

        # Start Hokuyo sensors
        res = vrep.simxSetIntegerSignal(
            self.client_id, 'handle_xy_sensor', 2, vrep.simx_opmode_oneshot)
        Vrepobject.check_for_errors(res, "Start Sensors, simxSetIntegerSignal")

        #  # Visualise sensor beams
        #  vrep.simxSetIntegerSignal(
        #      self.client_id, 'displaylasers', 1, vrep.simx_opmode_oneshot)

        # Get sensor handles
        self.hokuyo1 = Sensor(self.client_id, 'fastHokuyo_sensor1')
        self.hokuyo2 = Sensor(self.client_id, 'fastHokuyo_sensor2')

        # Total angle is 240degs 
        # but we say that in front of the robot there is 0 deg
        # left of the robot is from 0 to -120 deg
        # rigth of the robot is from 0 to 120 deg

        self.view_angle = np.deg2rad(240)
        self.half_view_angle = self.view_angle / 2

        self.sensor_data = []
        self.sensor_data = self.get_sensor_data()

    def get_distance_of_angle(self, angle):
        if abs(angle) > self.half_view_angle:
            log.error(f"Angle {angle} > {self.half_view_angle}")
            raise ValueError("Angle too big")

        index = self.get_index_to_angle(angle)
        log.debug(f"Angle {angle} refers to sensor_data[{index}]")
        return self.sensor_data[index]

    def get_angle_to_index(self, index):
        if index < 0 or index > len(self.sensor_data):
            raise ValueError("Wrong index {index}")

        num_datapoints = len(self.sensor_data)
        rad_per_index = self.view_angle / num_datapoints
        angle_l2r = index * rad_per_index
        angle = angle_l2r - self.half_view_angle
        return angle

    def get_index_to_angle(self, angle):
        """
        Get sensor_data index to given angle (angle must be element of {-4/6*pi,
        4/6*pi}) 
        :param angle: angle in radians
        :return index: index of sensor_data point of the angle
        """
        if angle < -self.half_view_angle or angle > self.half_view_angle:
            raise ValueError(f"Wrong angle {angle}")

        angle_l2r = angle + self.half_view_angle
        num_datapoints = len(self.sensor_data)
        index = int(np.round(num_datapoints * angle_l2r /
                             self.view_angle))
        return index

    def update_sensor_data(self):
        # Get transformed sensor data of both sensors
        sensor_data1 = self.hokuyo1.get_sensor_data_relative_to(
                self.robot_handler)
        sensor_data2 = self.hokuyo2.get_sensor_data_relative_to(
                self.robot_handler)
        sensor_data1 = [datum[3] for datum in sensor_data1[::-1]]
        sensor_data2 = [datum[3] for datum in sensor_data2[::-1]]

        # append sensor 1 to sensor 2 to make it look like single sensor
        sensor_data = np.append(sensor_data2, sensor_data1)
        if len(sensor_data) > 0:
            self.sensor_data = sensor_data
        #  log.debug(f"Sensor data:\n {self.sensor_data}")

    def get_sensor_data(self):
        self.update_sensor_data()
        return self.sensor_data

    def get_front_available_distance(self):
        dists = self.get_front_distances()
        return sum(dists)/len(dists)
        d1, d2 = self.get_front_distances()
        return (d1 + d2)/2

    def get_right_available_distance(self):
        dists = self.get_right_distances()
        return sum(dists)/len(dists)

    def get_left_available_distance(self):
        dists = self.get_left_distances()
        return sum(dists)/len(dists)

    def get_left_distances(self):
        #  self.plot_sensor_data(sensor_data)
        left_max = np.deg2rad(-85)
        left_min = np.deg2rad(-95)
        dists = self.get_distances_from_to_angle(left_min, left_max)
        return dists

    def get_distances_from_to_angle(self, a_from, a_to):
        a_min = a_from if a_from < a_to else a_to
        a_max = a_from if a_from >= a_to else a_to
        one_degree = np.deg2rad(1)
        dists = []
        i = a_min
        while i < (a_max+one_degree):
            dist = self.get_distance_of_angle(i)
            dists.append(dist)
            i += one_degree
        return dists

    def get_right_distances(self):
        right_min = np.deg2rad(85)
        right_max = np.deg2rad(95)
        dists = self.get_distances_from_to_angle(right_min, right_max)
        return dists

    def get_front_distances(self):
        center_min = np.deg2rad(-5)
        center_max = np.deg2rad(5)
        dists = self.get_distances_from_to_angle(center_min, center_max)
        return dists

    def get_distance_of_index(self, index):
        dist = self.sensor_data[index]
        return dist[3]

    def get_angle_with_min_dist(self):
        """
        return minimum distance and angle of this distance
        """
        min_dist = 5
        index = 0
        for i, value in enumerate(self.sensor_data):
            if value < min_dist:
                min_dist = value
                index = i

        angle = self.get_angle_to_index(index)
        return (min_dist, angle)

    def plot_sensor_data(self):
        sensor_data = self.sensor_data
        fig = plot.figure()
        ax = fig.add_subplot(1, 1, 1)
        plot.scatter(sensor_data[:, 0], sensor_data[:, 1])
        robowidth = self.D
        robolength = self.C+self.B
        xy = (-robowidth/2.0, -robolength/2.0)
        robot = plot.Rectangle(xy, robowidth, robolength, color='r')
        ax.add_patch(robot)
        # plot sensor ray with minimal distance
        hokuyo_idx_min_dist = np.argmin(sensor_data[:,3])
        x = sensor_data[hokuyo_idx_min_dist][0]
        y = sensor_data[hokuyo_idx_min_dist][1]
        # Leftmost sensor
        x1 = sensor_data[0][0]
        y1 = sensor_data[0][1]
        # rightmost sensor
        x3 = sensor_data[683][0]
        y3 = sensor_data[683][1]
        r1_x = sensor_data[self.hokuyo_idx_r1][0]
        r1_y = sensor_data[self.hokuyo_idx_r1][1]
        r2_x = sensor_data[self.hokuyo_idx_r2][0]
        r2_y = sensor_data[self.hokuyo_idx_r2][1]
        # sensor range for the left side
        l1_x = sensor_data[self.hokuyo_idx_l1][0]
        l1_y = sensor_data[self.hokuyo_idx_l1][1]
        l2_x = sensor_data[self.hokuyo_idx_l2][0]
        l2_y = sensor_data[self.hokuyo_idx_l2][1]
        # Plot sensor rays
        plot.plot([0, x], [0, y], 'r-')
        plot.plot([0, x1], [0, y1], 'g-')
        plot.plot([0, x3], [0, y3], 'g-')
        plot.plot([0, r1_x], [0, r1_y], 'y-')
        plot.plot([0, r2_x], [0, r2_y], 'y-')
        plot.plot([0, l1_x], [0, l1_y], 'y-')
        plot.plot([0, l2_x], [0, l2_y], 'y-')
        # Plot labels
        labels = ('-5', '', '-4', '', '-3', '', '-2', '', '-1', '', '0', '',
                            '1', '', '2', '', '3', '', '4', '', '5')
        plot.xticks(np.arange(-5, 5, 0.5), labels)
        plot.yticks(np.arange(-5, 5, 1))
        plot.xlabel("x")
        plot.ylabel("y")
        plot.show()
