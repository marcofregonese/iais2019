# Introduction to Intelligent and Autonomus Systems, UIBK, 2017
# contact:senka.krivic@uibk.ac.at
import numpy as np
import traceback
import vrep
from robot import Robot
from vrepobject import Vrepobject
from robotcontroller import Robotcontroller
from distbug import DistBug
from check_goal_pos_behavior import CheckGoalPosBehavior
from pick_up_object_behavior import PickUpObjectBehavior
import traceback
import logging

log = logging.getLogger('Main')


##########
#        #   
# EXAM 3 #
#        #
##########
'''
 Goal : tidy up the environment by putting colored blocks into the baskets.

    Use this scene: examination3_2018_update.ttt
    Using color-based object detection methods, explore the environment to find the colored blocks.
    Bring all the found blocks of ONE color into ONE chosen basket and blocks of the other colour into the other basket (pick which basket correspond to each color at the beginning)

Expected methods:

    Use a state machine to control the different steps in the robot's behaviour.
    Use at least these 3 arm poses when controlling the arm: rest pose (the whole arm lies into the surface occupied by the robot base ; it is used when the robot moves in the environment), grasping pose (the arm is next to the target object and closing the gripper allows to grasp it ; obviously used when grasping), drop pose (the arm is extended to place the object out of the surface occupied by the robot base). You can use additional poses if needed.
    The method to move the robot in the environment is of your choice, just don't harm the peaceful wandering robots by colliding with them.
    Bonus point: use a method based on the computed homography to get world coordinates of detected objects and reach them.

Minimal expected outputs:

    A terminal output of the basket selected to be filled up and which basket is associated with which colour (e.g. basket1 : red, basket2 : blue)
    A terminal output of the current state of the robot (from the state-machine) and the transition that is triggered (name of the state/transition) when changing state. Naming is at your choice but should describe the robot behaviour.
    A global view (graph) of your state machine representing all states and transitions (as a separate figure).
    A terminal output of the arm pose names and joint configuration (e.g. rest pose : [0.12, 0.8, …])
    demonstration of the robot performing the task (simulation)

Hints:

    If you use homography, use the chessboard to find the initial matrix and then keep track of the modification given robot displacements
    Use robot position from the simulator
'''


def main():
    """
    Main function
    """
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('Proximitysensor').setLevel(logging.INFO)
    logging.getLogger('Robot').setLevel(logging.INFO)
    logging.getLogger('RobotArm').setLevel(logging.DEBUG)
    logging.getLogger('RobotStateMachine').setLevel(logging.INFO)
    logging.getLogger('GoToGoalBehavior').setLevel(logging.INFO)
    logging.getLogger('CheckGoalPosBehavior').setLevel(logging.DEBUG)
    logging.getLogger('PickUpObjectBehavior').setLevel(logging.DEBUG)
    logging.getLogger('RgbSensor').setLevel(logging.DEBUG)
    print('Program started')
    vrep.simxFinish(-1)     # just in case, close all opened connections
    clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 2000, 5)
    if clientID != -1:
        print('Connected to remote API server')

        # Start the simulation:
        vrep.simxStartSimulation(clientID, vrep.simx_opmode_oneshot_wait)
        try:

            rob = Robot(clientID)
            ctrl = Robotcontroller(rob)
            ctrl.robot.setVelocity(6)

            pos = [-0.075, 0.200, 0.085]
            ctrl.current_goal = pos
            pick_up = PickUpObjectBehavior(ctrl)
            pick_up.execute()
            exit(0)
            # move the robot to visualize the chessboard
            ctrl.robot.sidewaysMeters(-0.5)

            ctrl.robot.get_homography()

            ctrl.execute()

        except:
            log.error("Something went wrong")
            traceback.print_exc()
        finally:
            # Stop simulation:
            vrep.simxStopSimulation(clientID, vrep.simx_opmode_oneshot_wait)

            # Now close the connection to V-REP:
            vrep.simxFinish(clientID)

    else:
        print('Failed connecting to remote API server')
    print('Program ended')


if __name__ == "__main__":
    main()
