import numpy as np
import vrep
from robot import Robot
from vrepobject import Vrepobject
from distbug import DistBug
import logging
import traceback

log = logging.getLogger('Main')


def main():
    logging.basicConfig(level=logging.DEBUG)
    print('Program started')
    vrep.simxFinish(-1)     # just in case, close all opened connections
    clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 2000, 5)
    if clientID != -1:
        try:
            print('Connected to remote API server')

            # emptyBuff = bytearray()

            # Start the simulation:
            vrep.simxStartSimulation(clientID, vrep.simx_opmode_oneshot_wait)

            rob = Robot(clientID)
            rob.setVelocity(4)
            #  rob.rotateDegrees(-22)
            #  rob.rotateDegrees(90)
            #  rob.forwardMeters(-0.3)
            #  rob.sidewaysMeters(-0.34)
            rob.sidewaysMeters(0.1)
            rob.test_arm()
            #  rob.find_red_objects()
        except:
            traceback.print_exc()
        finally:
            # Stop simulation:
            vrep.simxStopSimulation(clientID, vrep.simx_opmode_oneshot_wait)

            # Now close the connection to V-REP:
            vrep.simxFinish(clientID)
    else:
        print('Failed connecting to remote API server')
    print('Program ended')


if __name__ == "__main__":
    main()
