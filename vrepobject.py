import vrep
import numpy as np
import logging
log = logging.getLogger('Vrepobject')

class Vrepobject(object):

    def __init__(self, id, name):
        self.client_id = id
        self.name = name
        self.handle = self.init_handle()
        res, self.base_pos = vrep.simxGetObjectPosition(
            self.client_id, self.handle, 
            -1, vrep.simx_opmode_streaming)
        res, self.base_ori = vrep.simxGetObjectOrientation(
            self.client_id, self.handle, 
            -1, vrep.simx_opmode_streaming)
        # print("vrepobject initialized name: ", self.name, " handle n: ", self.handle)

    def get_handle(self):
        return self.handle

    def init_handle(self):
        # Get object handle
        res, handle = vrep.simxGetObjectHandle(
            self.client_id, self.name, vrep.simx_opmode_oneshot_wait)
        Vrepobject.check_for_errors(res, "Init handle")
        return handle

    def get_position_relative_to(self,
                                 relativeToHandle,
                                 opmode=vrep.simx_opmode_buffer):
        base_pos = vrep.simxGetObjectPosition(
                          self.client_id,
                          self.handle,
                          relativeToHandle,
                          opmode)
        # make sure all streaming data has reached client at least once
        vrep.simxGetPingTime(self.client_id)
        return base_pos[1]

    def get_position(self, opmode=vrep.simx_opmode_buffer):
        return self.get_position_relative_to(-1, opmode)

    def get_orientation_relative_to(self, relative_to_handle):
        base_orient = vrep.simxGetObjectOrientation(
                            self.client_id,
                            self.handle,
                            relative_to_handle,
                            vrep.simx_opmode_oneshot_wait)
        # make sure all streaming data has reached client at least once
        vrep.simxGetPingTime(self.client_id)
        return base_orient[1]

    def get_egocentric_transformation_matrix(self):
        """
            transfrom the ground truth values in the world frame 
            to the objects body egocentric frame
        """
        # build rotation matrix
        self_orientation = self.get_orientation()
        gamma = self_orientation[2]
        c, s = np.cos(-gamma), np.sin(-gamma)
        rotM = np.array([[c, -s, 0],
                         [s, c, 0],
                         [0, 0, 1]])
        # build translation matrix
        self_position = self.get_position()
        x = self_position[0]
        y = self_position[1]
        translationM = np.array([[1, 0, -x],
                                 [0, 1, -y],
                                 [0, 0, 1]])
        transformM = rotM.dot(translationM)
        return transformM

    def get_UNDOegocentric_transformation_matrix(self):
        self_orientation = self.get_orientation()
        gamma = self_orientation[2]
        c, s = np.cos(gamma), np.sin(gamma)
        rotM = np.array([[c, -s, 0],
                         [s, c, 0],
                         [0, 0, 1]])
        self_position = self.get_position()
        x = self_position[0]
        y = self_position[1]
        translationM = np.array([[1, 0, x],
                                 [0, 1, y],
                                 [0, 0, 1]])
        transformM = translationM.dot(rotM)
        return transformM

    def get_orientation(self):
        return self.get_orientation_relative_to(-1)

    def get_orientationDeg(self):
        a_b_g = self.get_orientation_relative_to(-1)
        a_deg = a_b_g[0]*360/(np.pi*2) 
        b_deg = a_b_g[1]*360/(np.pi*2) 
        g_deg = a_b_g[2]*360/(np.pi*2) 
        return [a_deg, b_deg, g_deg]

    @staticmethod
    def check_for_errors(return_value, function_name):
        if return_value == vrep.simx_return_ok:
            return
        if return_value == vrep.simx_return_novalue_flag:
            log.debug("%s: There is no command reply in the input buffer. " %
                    (function_name))
            return
        if return_value == vrep.simx_return_timeout_flag:
            log.error("%s: The function timed out (probably the network is down or too \
                slow)" % (function_name))
        if return_value == vrep.simx_return_illegal_opmode_flag:
            log.error("%s: The specified operation mode is not supported for the given \
                function" % (function_name))
        if return_value == vrep.simx_return_remote_error_flag:
            log.error("%s: The function caused an error on the server side (e.g. an \
                invalid handle was specified)" % (function_name))
        if return_value == vrep.simx_return_split_progress_flag:
            log.error("%s: The communication thread is still processing previous split \
                command of the same type" % (function_name))
        if return_value == vrep.simx_return_local_error_flag:
            log.error("%s: The function caused an error on the client side" % \
                (function_name))
        if return_value == vrep.simx_return_initialize_error_flag:
            log.error("%s: simxStart was not yet called" % (function_name))
        exit(1)

