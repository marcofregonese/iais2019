import numpy as np
import logging

log = logging.getLogger('FindBlobsBehavior')


class FindBlobsBehavior():
    def __init__(self, controller):
        self.controller = controller
        self.robot = self.controller.robot
        self.follow_dist = 0.6
        # from center of robot
        # center is on 0 degrees
        # left is on -90 degrees 
        # right is no 90 degrees
        self.left_low = np.deg2rad(-95)
        self.left = np.deg2rad(-90)
        self.left_high = np.deg2rad(-85)

    def execute(self):
        ctrl = self.controller

        # always look for blobs first
        object_found = ctrl.look_for_object()
        if object_found:
            return
        else:
            # check distances to obstacles
            min_dist = ctrl.get_min_sensor_dist()
            min_dist_angle = ctrl.get_angle_of_min_sensor_dist()
            log.debug(f"min_dist {np.round(min_dist, 2)}")
            log.debug(f"at angle {np.round(np.rad2deg(min_dist_angle), 2)}")

            if(self.no_obstacle_to_follow(min_dist) or
               self.is_parallel_to_obstacle(min_dist_angle)):
                self.go_straight(self.follow_dist)
            else:
                corr_angle = self.left - min_dist_angle
                log.debug(f"correction: {np.round(np.rad2deg(corr_angle), 2)}")
                # correct angle
                ctrl.rotate_angle_controller(corr_angle)
                self.go_straight(self.follow_dist)

    def go_straight(self, dist):
        self.controller.move_dist_controller(dist)

    def no_obstacle_to_follow(self, min_dist):
        return min_dist > 2 * self.follow_dist

    def is_parallel_to_obstacle(self, angle):
        return angle > self.left_low and angle < self.left_high


class FindBlobStates():
    GO_STRAIGHT = 1
    FOLLOW_WALL = 2
