
# Introduction to Intelligent and Autonomus Systems, UIBK, 2017
# contact:senka.krivic@uibk.ac.at
import vrep
from robotcontroller import Robotcontroller
from robot import Robot
from vrepobject import Vrepobject
from distbug import DistBug
import logging
import traceback

log = logging.getLogger('Main')


def main():
    logging.basicConfig(level=logging.DEBUG)
    print('Program started')
    vrep.simxFinish(-1)     # just in case, close all opened connections
    clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 2000, 5)
    if clientID != -1:
        print('Connected to remote API server')

        # emptyBuff = bytearray()

        # Start the simulation:
        vrep.simxStartSimulation(clientID, vrep.simx_opmode_oneshot_wait)

        try:
            rob = Robot(clientID)
            print(f"robot object ref:{rob}")
            rob.setVelocity(3)
            rob.doSomething()
            rob.set_Goal('redCylinder5')
            rob.rotate_towards_goal()

            controller = Robotcontroller(rob)
            redCylinder2 = [0.4249, 1.6750, 0]
            redCylinder4 = [2.9749, 5.6750, 0]
            redCylinder5 = [6.2499, 0.4500, 0]
            controller.set_goal_pos(redCylinder2)
            controller.set_goal_pos(redCylinder4)
            controller.pd_controller(redCylinder5, 1, 1)
            #controller.move_robot(5, 3)
        except:
            traceback.print_exc()

        # Stop simulation:
        vrep.simxStopSimulation(clientID, vrep.simx_opmode_oneshot_wait)

        # Now close the connection to V-REP:
        vrep.simxFinish(clientID)
    else:
        print('Failed connecting to remote API server')
    print('Program ended')


if __name__ == "__main__":
    main()
