import numpy as np
import utils as Utils
import cv2
import logging

log = logging.getLogger('Homography')

world_ground_points_old = [
                 [-0.025, 0.125],
                 [-0.075, 0.125],
                 [-0.125, 0.125],
                 [-0.175, 0.125],

                 [-0.025, 0.075],
                 [-0.075, 0.075],
                 [-0.125, 0.075],
                 [-0.175, 0.075],

                 [-0.025, 0.025],
                 [-0.075, 0.025],
                 [-0.125, 0.025],
                 [-0.175, 0.025]]


def get_chessboard_world_ground_points():
    world_ground_points = []
    for pt in world_ground_points_old:
        #  pt = np.array(pt) + np.array([0.1, +0.425])
        pt = np.array(pt)
        world_ground_points.append(pt)
    return world_ground_points


def getAi(X, x):
    """
     functions of algorithm to obtain H (see manual page 91) X = Hx (X=x')

     :param X: ground point coordinates,
     :param x: pixel coordinates #(i) For each correspondence
    xi ↔ x'i compute the matrix Ai from (4.1). 
     Only the first two rows need be used in general.
    """
    A = np.array([[0, 0, 0, -x[0], -x[1], -1, X[1]*x[0], X[1]*x[1], X[1]],
                  [x[0], x[1], 1, 0, 0, 0, -X[0]*x[0], -X[0]*x[1], -X[0]]])
    # print(f"Ai:\n{A}")
    return A


def assembleAs(A, Ai):
    """
    (ii) Assemble the n 2 × 9 matrices Ai into a single 2n × 9 matrix A.
    """
    As = np.vstack((A, Ai))
    return As

def getSVDofA(A):
    """ 
    (iii) Obtain the SVD of A (section A4.4(p585)). The unit singular
    vector corresponding to the smallest singular value is the solution h.
    Specifically, if A = UDV T with D diagonal with positive diagonal
    entries, arranged in descending order down the diagonal, then h is the
    last column of V. """
    return np.linalg.svd(A)


def homography(corners, sensor):
    """
    all together
    """
    sensor_gps = []
    A = []
    world_ground_points = get_chessboard_world_ground_points()
    for wgp in world_ground_points:
        sensor_gp = transformGroundPointToSensorEgoCentricFrame(wgp, sensor)
        sensor_gps.append(sensor_gp)

    idx = 0
    for sgp in sensor_gps:
        Ai = getAi(sgp, corners[idx][0])
        if idx == 0:
            A = Ai
        else:
            A = assembleAs(A, Ai)
        idx += 1

    UDV = getSVDofA(A)
    UDV[2][8][8] = 1
    ret = np.array(UDV[2].round(3)[8])
    ret.shape = (3, 3)
    log.debug(f"homography:\n{ret}")
    return ret


def transformGroundPointToSensorEgoCentricFrame(pt, sensor):
    vector = np.array(Utils.homogeneous2Dto3D(pt))
    matrix = sensor.get_egocentric_transformation_matrix()
    result = matrix.dot(vector)
    # print(f"tranformed ground truth value:\n{result}")
    return result


def homographyCV2(corners, sensor):
    pts_src = []
    pts_dst = []
    for corner in enumerate(corners):
        pts_src.append(corner[1])
    pts_src = np.array(pts_src)
    world_ground_points = get_chessboard_world_ground_points()

    for wgp in world_ground_points:
        sensor_gp = transformGroundPointToSensorEgoCentricFrame(wgp, sensor)
        pts_dst.append(sensor_gp)
    pts_dst = np.array(pts_dst)
    # print(f"pts_dst:\n{pts_dst}")
    h, status = cv2.findHomography(pts_src, pts_dst, cv2.RANSAC, 5.0)
    log.debug(f"homography CV2:\n{h.round(3)}")
    return h
