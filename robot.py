import time
import vrep
import numpy as np
import utils as Utils
from rgb_sensor import RgbSensor
from proximity_sensor import Proximitysensor
from vrepobject import Vrepobject
import homography as homogr
from wheel import Wheel
from robotarm import RobotArm
import logging

log = logging.getLogger('Robot')


class Robot(Vrepobject):
    # http://www.youbot-store.com/wiki/index.php/YouBot_Detailed_Specifications
    B = 0.10                # Wheel Diameter
    wheel_radius = B/2.0    # Wheel Radius
    tread_width = 0.30046  # distance btw center points of left and right wheel
    wheelbase = 0.471      # distance btw center of front and rear wheel
    length = 0.58
    G = (tread_width + wheelbase)/2.0
    velocity = 1.0
    # Velocity of wheels (omega_0, omega_1, omega_2, omega_3)

    def __init__(self, clientID):
        Vrepobject.__init__(self, clientID, 'youBot_center')
        self.client_id = clientID
        # Init Robot
        self.wheel_joints = self.init_robot()
        self.wv = [0.0, 0.0, 0.0, 0.0]  # wheel velocities

        # Get Proximity sensor
        self.proximity_sensor = Proximitysensor(clientID, self)

        # turn on rgb camera
        vrep.simxSetIntegerSignal(
            clientID, 'handle_rgb_sensor', 2, vrep.simx_opmode_oneshot)

        # Get rgb sensor handle
        self.rgbdSensorBody = RgbSensor(clientID, 'rgbdSensorBody')
        self.rgb_sensor = RgbSensor(clientID, 'rgbSensor')
        self.rgb_sensor.set_perspective_angle(45)

        # Get robot arm handle
        self.arm = RobotArm(clientID)
        self.H2 = None

    def test_arm(self):
        self.arm.move_arm_to([-0.20, 0.15, 0.075])
        time.sleep(2)
        gripper_pos = self.arm.gripper_tip.get_position()
        log.debug(f"Gripper Pos: {gripper_pos}")

    def get_homography(self):
            corners = self.rgb_sensor.get_chessboardCorners()
            H2 = []

            if(corners is not None):
                log.debug(f"chessboard corners:\n{corners}")
                H2 = homogr.homographyCV2(corners, self.rgbdSensorBody)
                #H2 = rob.homography(corners, self.rgb_sensor)
            else:
                raise Exception(f"No chessboard corners found")
            self.H2 = H2
            return H2

    def get_gp(self, H, colorRGB, color):
        """
        get ground point of colored objects

            :param H: homography
            :param colorRGB: color in rgb format
            :param color: string colorname
        """
        ret0 = self.mapImgToGround(H, colorRGB)
        res = []
        matrix = self.rgbdSensorBody.get_UNDOegocentric_transformation_matrix()
        for i in ret0:
            vector = np.array(Utils.homogeneous2Dto3D(i))
            result = matrix.dot(vector)
            res.append([color,
                        [float(format(result[0], '.2f')),
                         float(format(result[1], '.2f'))]])
        return res

    def mapImgToGround(self, H, color):
        imgxy = self.rgb_sensor.get_imgObjBottomCoordinates(color)
        log.debug(f"imgxy:{imgxy}")
        ret = []
        for i in imgxy:
            i.append(1)
            vec3 = np.array(i)
            gp = H.dot(vec3)
            x_y = [gp[0]/gp[2], gp[1]/gp[2]]
            #log.debug(f"x_y:{x_y}")
            ret.append([float(format(x_y[0], '.2f')),
                        float(format(x_y[1], '.2f'))])
            #log.debug(f"x y:{x_y}")
        return ret

    def get_global_velocities(self):
        # From https://seeeddoc.github.io/4WD_Mecanum_Wheel_Robot_Kit_Series/
        v_x = self.wheel_radius / 4 * (self.wv[2] + self.wv[1]
                                       + self.wv[3] + self.wv[0])
        v_y = self.wheel_radius / 4 * (-self.wv[2] + self.wv[1]
                                       + self.wv[3] - self.wv[0])
        w = self.wheel_radius / (4*self.G) * (-self.wv[2] + self.wv[1]
                                              - self.wv[3] + self.wv[0])
        return [v_x, v_y, w]

    def move(self, wheelVel, mv_time):
        vrep.simxPauseCommunication(self.client_id, True)
        self.wv = wheelVel

        for i in range(0, 4):
            vrep.simxSetJointTargetVelocity(
                self.client_id,
                self.wheel_joints[i],
                self.wv[i],
                vrep.simx_opmode_oneshot)
        vrep.simxPauseCommunication(self.client_id, False)
        time.sleep(mv_time)
        # let the robot stop after sleeping, at least for testing 
        # so i can see where the robot is
        vrep.simxPauseCommunication(self.client_id, True)
        for i in range(0, 4):
            vrep.simxSetJointTargetVelocity(
                self.client_id,
                self.wheel_joints[i],
                0,
                vrep.simx_opmode_oneshot)
        vrep.simxPauseCommunication(self.client_id, False)

    def rotateRadians(self, rad):
        """
        definitive function to make youbot rotate in radians
        wheel speed  error in degrees (for pi rad rotation)
                 1        <1
                 5       >1,<2
                10       >2.55,<3
        """
        vel = self.wheelVel(0, 0, self.velocity)
        # set wheel velocity
        self.wv = vel
        # formula to get youbot angular velocity
        w = self.wheel_radius / (4*self.G) * (-self.wv[2] + self.wv[1] - 
                                              self.wv[3] + self.wv[0])
        # get time from radians/youbot angular velocity
        t = abs(rad) / w
        # adjust counterclockwise direction
        if rad > 0:
            vel *= -1
        self.move(vel, t)

    def rotateDegrees(self, deg):
        rad = np.pi*2*deg/360
        self.rotateRadians(rad)

    def forwardMeters(self, meters):
        t = abs(meters) / (self.velocity * self.wheel_radius)
        v = self.wheelVel(self.velocity, 0, 0)
        if meters < 0:
            v *= -1
        self.move(v, t)

    def sidewaysMeters(self, meters):
        t = abs(meters) / (self.velocity * self.wheel_radius)
        v = self.wheelVel(0, self.velocity, 0)
        if meters > 0:
            v *= -1
        self.move(v, t)

    def setVelocity(self, vel):
        log.debug(f"set velocity to {vel}")
        self.velocity = vel

    def wheelVel(self, forwBackVel, leftRightVel, rotVel):
        # links vorne, links hinten, rechts hinten, rechts vorne
        wheelVelocities = np.array(
                [forwBackVel - leftRightVel + rotVel,
                 forwBackVel + leftRightVel + rotVel,
                 forwBackVel - leftRightVel - rotVel,
                 forwBackVel + leftRightVel - rotVel])
        return wheelVelocities

    def get_quadrant_wrt(self, pos_goal):
        pos_robot = self.get_position()
        rx = pos_robot[0]
        ry = pos_robot[1]
        tx = pos_goal[0]
        ty = pos_goal[1]
        res = (rx*-1+tx, ry*-1+ty)
        quadrant = 0
        if res[0] >= 0 and res[1] >= 0:
            quadrant = 1
        elif res[0] < 0 and res[1] >= 0:
            quadrant = 2
        elif res[0] < 0 and res[1] < 0:
            quadrant = 3
        elif res[0] >= 0 and res[1] < 0:
            quadrant = 4
        return quadrant

    # definitive function to get goal direction
    def get_goal_direction(self, goal_pos):
        a_b_g = self.get_orientation()
        g = a_b_g[2]
        quadrant = self.get_quadrant_wrt(goal_pos)
        dist_x, dist_y, dist_abs = self.get_triangle_to(goal_pos)
        # log.debug("x ", dist_x, " y ", dist_y, " distance ", dist_abs)
        if quadrant == 1:
            angle = np.pi - np.arcsin(dist_x/dist_abs) - g
        elif quadrant == 2:
            angle = - np.pi + np.arcsin(dist_x/dist_abs) - g
        elif quadrant == 3:
            angle = - np.arcsin(dist_x/dist_abs) - g
        elif quadrant == 4:
            angle = np.arcsin(dist_x/dist_abs) - g
        # log.debug("Angle towards goal:", angle*360/(2*np.pi))
        return angle

    def get_angluar_velocity(self, wheel_velocity):
        wv = wheel_velocity
        R = self.wheel_radius
        omega = R / (4*self.G) * (-wv[2] + wv[1] - wv[3] + wv[0])
        log.debug(f"-{wv[2]} + {wv[1]} - {wv[3]} + {wv[0]}")
        return omega

    def get_wheel_vel_from_angluar(self, omega):
        wv = omega * self.G / self.R
        return [wv, wv, -wv, -wv]

    def rotate_towards_goal(self, goal_pos):
        rot_angle = self.get_goal_direction(goal_pos)
        log.debug(f"rot_angle {rot_angle}")
        self.rotateRadians(rot_angle)

    def get_front_free_distance(self):
        rot_angle = self.get_goal_direction()
        self.rotateRadians(rot_angle)
        distance = self.get_front_available_distance()
        self.rotateRadians(-1*rot_angle)
        return distance

    def get_distance_to(self, pos_goal):
        return self.get_triangle_to(pos_goal)[0]

    def get_triangle_to(self, pos_goal):
        """
        returns, dist_x, dist_y, and abs_dist (triangle) to pos_goal
        """
        pos_robot = self.get_position()
        target_relative_to_robot = np.array(pos_goal) - np.array(pos_robot)
        # Distance in x direction
        dist_x = target_relative_to_robot[0]
        # Distance in x direction
        dist_y = target_relative_to_robot[1]
        # Absolute distance ( d = sqrt(x^2 + y^2))
        dist_abs = Utils.get_pythagoras_distance(dist_x, dist_y)
        return dist_x, dist_y, dist_abs

    def get_sensors_data(self):
        return self.proximity_sensor.get_sensor_data()

    def init_robot(self):
        wheel_joints = np.empty(4, dtype=np.int)
        wheel_joints.fill(-1)

        # front left, rear left, rear right, front right
        _, wheel_joints[2] = vrep.simxGetObjectHandle(
            self.client_id,
            'rollingJoint_fl',
            vrep.simx_opmode_oneshot_wait)
        _, wheel_joints[3] = vrep.simxGetObjectHandle(
            self.client_id,
            'rollingJoint_rl',
            vrep.simx_opmode_oneshot_wait)
        _, wheel_joints[0] = vrep.simxGetObjectHandle(
            self.client_id,
            'rollingJoint_rr',
            vrep.simx_opmode_oneshot_wait)
        _, wheel_joints[1] = vrep.simxGetObjectHandle(
            self.client_id,
            'rollingJoint_fr',
            vrep.simx_opmode_oneshot_wait)
        # set wheel velocity to 0
        for i in range(0, 4):
            vrep.simxSetJointTargetVelocity(
                self.client_id,
                wheel_joints[i],
                0,
                vrep.simx_opmode_oneshot)
        return wheel_joints
